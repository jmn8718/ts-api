interface EnvConfig {
  PORT: number;
  ENV: string;
}

interface AppConfig {
  MORGAN_LEVEL: string;
  LOGGER_LEVEL: string;
}

interface DbConfig {
  type: string;
  host: string;
  username: string;
  password: string;
  database: string;
  port: number;
  logging: boolean;
  synchronize: boolean;
}
export const CONFIG: EnvConfig = {
  PORT: parseInt(process.env.PORT || "3000"),
  ENV: process.env.NODE_ENV || "development"
};

export const APP_CONFIG: AppConfig = {
  MORGAN_LEVEL: process.env.MORGAN_LEVEL || CONFIG.ENV === "production" ? "combined" : "dev",
  LOGGER_LEVEL: process.env.LOGGER_LEVEL || "trace"
};

const handleBooleanString = function (stringValue: string = "", defaultValue: boolean): boolean {
  if (stringValue.toLowerCase() === "true") {
    return true;
  } else if (stringValue.toLowerCase() === "false") {
    return false;
  }
  return defaultValue;
};

export const DB_CONFIG: DbConfig = {
  "type": process.env.DB_TYPE || "postgres",
  "host": process.env.DB_HOST || "localhost",
  "port": parseInt(process.env.DB_PORT || "5432"),
  "username": process.env.DB_USERNAME || "developer",
  "password": process.env.DB_PASSWORD || "d3v3l0p3r",
  "database": process.env.DB_DATABASE || "db",
  "synchronize": handleBooleanString(process.env.DB_SYNCHRONIZE, true),
  "logging": handleBooleanString(process.env.DB_LOGGING, false)
};