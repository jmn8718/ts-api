import { createConnection, Connection, getConnectionOptions, ConnectionOptions } from "typeorm";
import { DB_CONFIG } from "../config";

let connection: Connection;

export const getConnection = function (): Connection {
  if (!connection) {
    throw new Error("Database has not been initialized");
  }
  return connection;
};

export const initializeDB = async function () {
  try {
    const connectionOptions: ConnectionOptions = await getConnectionOptions();

    for (const currentKey in DB_CONFIG) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      connectionOptions[currentKey] = DB_CONFIG[currentKey];
    }

    connection = await createConnection(connectionOptions);
  } catch (err) {
    process.exit(1);
  }
};