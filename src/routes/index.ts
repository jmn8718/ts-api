import { Application, Request, Response } from "express";

export function registerRoutes(app: Application) {
  app.get("/health", function (_: Request, res: Response) {
    return res.json({ status: true });
  });

  app.get("/", function (_: Request, res: Response) {
    return res.json({ success: true });
  });
}